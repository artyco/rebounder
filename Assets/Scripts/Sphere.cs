using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sphere : MonoBehaviour
{
    public Rigidbody2D rbody;
    public Animator anim;
    private AudioSource audioSource;

    void Awake() {
        audioSource = this.GetComponent<AudioSource>();
    }

    private void OnCollisionEnter2D(Collision2D collision){
        audioSource.Play();
        if (collision.gameObject.CompareTag("Player") || collision.gameObject.CompareTag("Enemy"))
            anim.SetTrigger("Hit");
    }
}
