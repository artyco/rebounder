using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class KnockBack : MonoBehaviour
{
    private Rigidbody2D rbody;

    [SerializeField]
    private float strength = 16, delay = 0.12f;

    public UnityEvent OnBegin, OnDone;

    void Awake() {
        rbody = this.GetComponent<Rigidbody2D>();
    }

    public void ContinuousKnockBack(GameObject sender) {
        Vector2 direction = (transform.position-sender.transform.position).normalized;
        rbody.AddForce(direction*strength, ForceMode2D.Impulse);
    }

    public void NormalKnockBack(GameObject sender) {
        StopAllCoroutines();
        OnBegin?.Invoke();
        Vector2 direction = (transform.position-sender.transform.position).normalized;
        rbody.AddForce(direction*strength, ForceMode2D.Impulse);
        StartCoroutine(Reset());
    }

    private IEnumerator Reset() {
        yield return new WaitForSeconds(delay);
        rbody.velocity = Vector3.zero;
        OnDone?.Invoke();
    }
}
