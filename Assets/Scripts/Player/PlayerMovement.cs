using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Events;
public class PlayerMovement : MonoBehaviour
{

    [Header("Events")]
    public UnityEvent FollowPlayer,FollowGroup;
    public UnityEvent OnSaberSwing;
    private bool followingP = false;

    [Header("Components")]
    public Rigidbody2D rbody;
    private Transform bolita,saber;
    public Animator anim, saberAnimator;
    private SaberAim saberAim;
    private TrailRenderer trailRenderer;
    public InputActionReference pointerPosition;
    private Vector2 pointerInput;
    private PlayerStats playerStats;
    private SoundManager soundManager;

    [Header("Movement Stats")]
    public float movSpeed = 6;
    public float currentSpeed = 0;
    [SerializeField] private bool isMoving;

    [Header("Jumping")]
    [SerializeField] private bool isJumping;
    public float jumpForce = 5;

    [Header("Collisions")]
    [SerializeField] public float groundCheckRadius, wallCheckRadius;
    [SerializeField] public Transform groundCheckCollider, wallCheckCollider;
    [SerializeField] public LayerMask groundLayer, wallLayer;

    [Header("Dashing")]
    [SerializeField] private float dashSpeed = 10f;
    [SerializeField] private float dashTime = 0.25f, dashColdown = 0.25f;
    [SerializeField] private bool isDashing, canDash=true;
    [SerializeField] private Vector2 lookDir;
 
    [Header("Stats")]
    private bool isRecharging, canThrow=true;

    void Awake()
    {        
        rbody = this.GetComponent<Rigidbody2D>();
        trailRenderer = this.GetComponent<TrailRenderer>();
        playerStats = this.GetComponent<PlayerStats>();
        soundManager = this.GetComponent<SoundManager>();
        saberAim = GetComponentInChildren<SaberAim>();
        saber = GameObject.Find("RotationPoint").GetComponent<Transform>();
        bolita = GameObject.FindGameObjectWithTag("Sphere").GetComponent<Transform>();        
    }


    void Update()
    {
        pointerInput = GetPointerInput();
        saberAim.PointerPosition = pointerInput;
        if(!isJumping) {
            anim.SetBool("Jump",false);
            anim.SetBool("Walk",isMoving);
        }
        else {
            anim.SetBool("Walk",false);
            anim.SetBool("Jump",true);
            if(rbody.velocity.y > 0) {
                anim.SetFloat("vSpeed",1);
            }
            else {                
                anim.SetFloat("vSpeed",-1);
            }
        }
        if(!isDashing && !isRecharging && playerStats.stamina<playerStats.maxStamina) {
            StartCoroutine(RechargeStamina());
        }
    }

    void FixedUpdate() 
    {
        OnGroundCheck();
        OnWallCheck();
        if(!isDashing)
            rbody.velocity = new Vector2(currentSpeed, rbody.velocity.y);
    }

    private Vector2 GetPointerInput() {
        Vector3 mousePos = pointerPosition.action.ReadValue<Vector2>();
        mousePos.z = Camera.main.nearClipPlane;
        return Camera.main.ScreenToWorldPoint(mousePos);
    }

    private void OnJump() {  
        if(!isJumping||anim.GetBool("WallJump")) {        
            soundManager.PlayClipByName("jump");
            Debug.Log("Jump");            
            rbody.velocity=new Vector2(rbody.velocity.x, jumpForce); 
            isJumping=true;       
        }           
    }

    private void OnMove(InputValue inputValue) {
        float moveValue = inputValue.Get<float>();
        lookDir.x = moveValue;
        Debug.Log("Move: " + moveValue);
        currentSpeed = moveValue * movSpeed;
        Vector2 scale = transform.localScale;
        Vector2 saberScale = saber.localScale;
        if(moveValue < 0 && transform.localScale.x>0) {      
            scale.x =-1;
            saberScale.x = -1;
        }        
        else if(moveValue > 0 && transform.localScale.x<0) {        
            scale.x = 1;
            saberScale.x =1;       
        }
        transform.localScale = scale;
        saber.localScale = saberScale;
        if(!isJumping)  {
            isMoving = true;         
            if(moveValue==0) {
                isMoving = false;  
            }
        }      
    }

    private void OnLook(InputValue inputValue) {
        float lookValue = inputValue.Get<float>();
        lookDir.y = lookValue;
        Debug.Log("Look: " + lookValue);
    }

    private void OnAttack() {
        if(canThrow) {
            OnSaberSwing?.Invoke();
            saberAnimator.SetTrigger("Attack");
        }            
    }

    private void OnThrow() {
        if(playerStats.mana >= 2 && canThrow) { 
            OnSaberSwing?.Invoke();           
            canThrow = false;
            saberAnimator.SetTrigger("Throw");
            StartCoroutine(EndThrow());
            playerStats.mana-=2;
            playerStats.MP.SetValue(playerStats.mana);
        }
    }

    private IEnumerator EndThrow() {
        yield return new WaitForSeconds(0.5f);
        canThrow = true;
    }

    private void OnDash() {
        if(canDash && playerStats.stamina >= 12) {
            if(lookDir.x!=0||lookDir.y!=0) {
                soundManager.PlayClipByName("dash");
                StartCoroutine(Dashing());
                playerStats.stamina -= 16;
                playerStats.SP.SetValue(playerStats.stamina);
            }
        }
    }

    private IEnumerator Dashing() {              
        isDashing = true;
        canDash = false;
        rbody.gravityScale = 0;
        trailRenderer.emitting = true;
        rbody.velocity = lookDir.normalized*dashSpeed;        
        yield return new WaitForSeconds(dashTime);
        trailRenderer.emitting = false;
        rbody.gravityScale = 0.5f;
        isDashing = false;
        yield return new WaitForSeconds(dashColdown);
        canDash = true;
    }

    private void OnTeleport() {
        if(playerStats.mana>=3) {
            soundManager.PlayClipByName("teleport");
            Debug.Log("Teleport");
            Vector2 aux1 = transform.position;
            Vector2 aux2 = bolita.transform.position;
            Vector2 vacio = new Vector2(0,-10);
            bolita.transform.position = vacio;
            transform.position = aux2;        
            bolita.transform.position = aux1;
            playerStats.mana-=3;
            playerStats.MP.SetValue(playerStats.mana);
        }        
    }

    /*private void OnCollisionEnter2D(Collision2D collision){
        if(collision.gameObject.CompareTag("Floor")) {
            isJumping = false; 
        }
    }*/

    private void OnGroundCheck() {
        Collider2D[] colliders = Physics2D.OverlapCircleAll(groundCheckCollider.position,groundCheckRadius,groundLayer);
        if(colliders.Length>0) {
            isJumping=false;
        }
        else
            isJumping=true;
    }

    private void OnWallCheck() {
        Collider2D[] colliders = Physics2D.OverlapCircleAll(wallCheckCollider.position,wallCheckRadius,wallLayer);
        if(colliders.Length>0&&isJumping)
            anim.SetBool("WallJump",true);
        else
            anim.SetBool("WallJump",false);
    }

    private void OnDrawGizmosSelected() {
        Gizmos.color = Color.blue;
        Vector3 position = groundCheckCollider == null ? Vector3.zero : groundCheckCollider.position;
        Gizmos.DrawWireSphere(position,groundCheckRadius);
        Gizmos.color = Color.yellow;
        position = wallCheckCollider == null ? Vector3.zero : wallCheckCollider.position;
        Gizmos.DrawWireSphere(position,wallCheckRadius);
    }

    public IEnumerator RechargeStamina() {
        isRecharging = true;
        playerStats.stamina+=2;
        playerStats.SP.SetValue(playerStats.stamina);
        yield return new WaitForSeconds(.5f);
        isRecharging = false;
    }

    private void OnChangeCamera() {
        if(!followingP) {
            followingP=true;
            FollowPlayer?.Invoke();
        }            
        else {
            followingP=false;
            FollowGroup?.Invoke();
        }
    }
}
