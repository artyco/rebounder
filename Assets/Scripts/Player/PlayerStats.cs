using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Events;

public class PlayerStats : MonoBehaviour
{
    public int health, maxHealth, mana, maxMana, stamina, maxStamina;
    public Bar HP, MP, SP;
    public GameObject deathParticles;
    private Animator anim;
    public UnityEvent OnDeath;
    private bool isDead = false;

    void Awake() {
        HP.SetMaxValue(maxHealth);
        MP.SetMaxValue(maxMana);
        SP.SetMaxValue(maxStamina);
        anim = this.GetComponent<Animator>();
    }

    private void OnCollisionEnter2D(Collision2D collision){
        if(collision.gameObject.CompareTag("Enemy")) {
            if(!isDead) {
                health-=5;
                HP.SetValue(health);
                anim.SetTrigger("Hurt");
                if(health<=0) {
                    Instantiate(deathParticles,transform.position,Quaternion.identity);
                    isDead=true;    
                    OnDeath?.Invoke();
                    anim.SetTrigger("Death");                                    
                    StartCoroutine(GameOver());
                }
            }
        }
        if(collision.gameObject.CompareTag("HealthStone")) {
            health++;
            HP.SetValue(health);
            Destroy(collision.gameObject);
        }
        if(collision.gameObject.CompareTag("ManaStone")) {
            mana++;
            MP.SetValue(mana);
            Destroy(collision.gameObject);
        }
    }

    private IEnumerator GameOver() {
        yield return new WaitForSeconds(0.4f);
        SceneManager.LoadScene(SceneManager.GetActiveScene ().buildIndex + 1);
        Destroy(gameObject);
    }

}
