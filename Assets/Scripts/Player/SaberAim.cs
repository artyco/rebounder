using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class SaberAim : MonoBehaviour
{
    public Vector2 PointerPosition { get; set; }

    public Transform circleOrigin;
    public float radius;

    void Update() {
        Vector2 direction = (PointerPosition-(Vector2)transform.position).normalized;
        transform.right = direction;
        Vector2 scale = transform.localScale;
        if(direction.x<0) {
            scale.y =-1;
        }
        else if(direction.x>0){
            scale.y = 1;           
        }
        transform.localScale = scale;
    }

    private void OnDrawGizmosSelected() {
        Gizmos.color = Color.red;
        Vector3 position = circleOrigin == null ? Vector3.zero : circleOrigin.position;
        Gizmos.DrawWireSphere(position,radius);
    }

    public void DetecColliders() {
        foreach (Collider2D collider in Physics2D.OverlapCircleAll(circleOrigin.position,radius))
        {
            Debug.Log("Collision: " + collider.name);
            KnockBack kb;
            if(kb = collider.GetComponent<KnockBack>()) {
                if(collider.gameObject.CompareTag("Sphere")) 
                    kb.ContinuousKnockBack(transform.parent.gameObject);
                else
                    kb.NormalKnockBack(transform.parent.gameObject);
            }                               
        }
    }
}
