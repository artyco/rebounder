using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

[System.Serializable]
public class Wave
{
    public string waveName;
    public int noOfEnemies;
    public GameObject[] typeOfEnemies;
    public float spawnInterval;
}

public class EnemySpawner : MonoBehaviour
{
    public Wave[] waves;
    public Transform[] spawnPoints;    
    private Wave currentWave;
    public Animator anim;
    public TMP_Text waveTxt;
    private int currentWaveNum;
    private float nextSpawnTime;
    [Header("Flags")]
    public bool canSpawn = true;

    private void Update() {
        currentWave = waves[currentWaveNum];
        SpawnWave();
        GameObject[] totalEnemies = GameObject.FindGameObjectsWithTag("Enemy");
        if(totalEnemies.Length == 0 && !canSpawn && currentWaveNum+1 != waves.Length) {
            waveTxt.text = waves[currentWaveNum + 1].waveName;
            anim.SetTrigger("WaveComplete");
        }
        else if(totalEnemies.Length == 0 && !canSpawn && currentWaveNum == waves.Length) {
            SceneManager.LoadScene(3);
        }
    }

    private void SpawnNextWave() {
        currentWaveNum++;
        canSpawn=true;
    }

    private void SpawnWave() {
        if(canSpawn && nextSpawnTime < Time.time) {
            GameObject enemy = currentWave.typeOfEnemies[Random.Range(0,currentWave.typeOfEnemies.Length)];
            Transform spoint = spawnPoints[Random.Range(0,spawnPoints.Length)];
            Instantiate(enemy,spoint.position,Quaternion.identity);
            currentWave.noOfEnemies--;
            nextSpawnTime = Time.time + currentWave.spawnInterval;
            if(currentWave.noOfEnemies==0) {
                canSpawn = false;
            }
        }        
    }

}
