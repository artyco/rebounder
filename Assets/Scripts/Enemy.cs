using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    private Transform target;
    private Rigidbody2D rbody;
    private SpriteRenderer spriteR;
    public GameObject enemyParticle;
    public GameObject deathSound;
    public GameObject[] stones;
    private float movSpeed;
    private int stoneType;
    public Vector2 movement;
    
    void Awake()
    {
        rbody = this.GetComponent<Rigidbody2D>();
        spriteR = this.GetComponent<SpriteRenderer>();
        target = GameObject.FindGameObjectWithTag("Player").transform;
        if(transform.position.x>target.position.x) {
            spriteR.flipY=true;
        }
        movSpeed = Random.Range(2,6);
        StoneChance();
    }

    private void StoneChance() {
        int numb = Random.Range(0,21);
        if(numb>=0 && numb <5) {
            stoneType = 0;
            spriteR.color = new Color(255, 153, 129, 255);
        }
        else if(numb>=5 && numb <10) {
            stoneType = 1;
            spriteR.color = new Color(148, 210, 238, 255);
        }
        else {
            stoneType = -1;
        }
            
    }

    void Update()
    {
        Vector2 direction = target.position - transform.position;
        float angle = Mathf.Atan2(direction.y,direction.x)*Mathf.Rad2Deg;     
        rbody.rotation = angle;
        direction.Normalize();
        MoveCharacter(direction);
    }

    private void MoveCharacter(Vector2 direction) {
        rbody.MovePosition((Vector2)transform.position + (direction * movSpeed * Time.deltaTime));                
    }

    private void OnCollisionEnter2D(Collision2D collision){
        if (collision.gameObject.CompareTag("Sphere") || collision.gameObject.CompareTag("Player")) { 
            Instantiate(enemyParticle,transform.position,Quaternion.identity);
            if(stoneType>=0)
                Instantiate(stones[stoneType],transform.position,Quaternion.identity);
            Instantiate(deathSound,transform.position,Quaternion.identity);
            Destroy(gameObject);                                     
        }
    }

}
