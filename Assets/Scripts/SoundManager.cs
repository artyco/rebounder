using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class AudioInfo 
{
    public string name;
    public AudioClip audioClip;
    public float volume = 1f;
    public float pitch = 1f;
}

public class SoundManager : MonoBehaviour
{
    private AudioSource audioSrc;
    public AudioInfo[] audios;
    private void Awake(){
        audioSrc = GetComponent<AudioSource>();
    }

    public void PlayCurrentClip(){
        audioSrc.Play();
    }


    public void PlayClipByName(string clipName) {
        SetCurrentAudioClip(clipName);
        PlayCurrentClip();
    }

    public void SetPitch(float pitch) {
        audioSrc.pitch = pitch;
    }

    public AudioInfo FindAudioInfoByName(string clipName) {
        foreach(AudioInfo audioInfo in audios) {
            if(audioInfo.name == clipName) {
                return audioInfo;
            }
        }
        return null;
    }

    public void SetCurrentAudioClip(string clipName) {
        AudioInfo audioInfo = FindAudioInfoByName(clipName);
        if(audioInfo == null) {
            throw new System.Exception(
                $"Not such an AudioInfo with the name \"{clipName}\"" +
                $"in the object {transform.name}."
            );
        }
        audioSrc.clip = audioInfo.audioClip;
        audioSrc.volume = audioInfo.volume;
        audioSrc.pitch = audioInfo.pitch;
    }


}
