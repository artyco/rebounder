using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostDeathSound : MonoBehaviour
{
    private AudioSource audioSource;
    void Awake()
    {
        audioSource = this.GetComponent<AudioSource>();
        audioSource.Play();
        StartCoroutine(GhostDeath());
    }

    private IEnumerator GhostDeath() {
        yield return new WaitForSeconds(audioSource.clip.length);
        Destroy(gameObject);
    }
    
}
